Summarization of papers
=======

Ontology-driven execution of clinical guidelines
----

This paper focuses on ontological management of guidelines.
Briefly it helps doctors to collect and manage information about patients
and coordinate complex tasks (e.g. scheduling meetings)

The ontology fills the semantic gap between an agent system and the representation
of medical knowledge in a formal language like PROforma.

A hybrid architecture for a preoperative decision support system using a rule engine and a reasoner on a clinical ontology.
-----

### Abstract:
System for preoperative risk assessment. System combines rule engine and reasoner which uses an
ontology developed decision support 

### Introduction:

- CPGs primary goal:  improve quality and efficiency of healthcare delivery while providing
an efficient, cost-effective and consistent service across healthcare institutions.

- CPGs consists of statements, protocols, rules, recommendations

- CPGs help professionals by suggesting specific advancement, for instance, in a therapy 

- CPGs refrain from overruling the professionals

- CPGs are created by a specific process

- reference to paper which take notes on cost-reduction possibilities by using guidelines

- benefits of clinical guidelines are acknowledged many times, but integrating results
in daily processes is difficult. Therefore Clinical Decision Support Systems (CDSS) are developed.

- Patient medical history is represented in OWL. Resoning tool is used to suggest appropriate
  preoperative tests based on NICE guideline.

### Decision support ontology and reasoning functionalities

Decision support provide advice to clinicians and flag potential risks.
Decision support is organised in two steps:

1. Calculating/Deriving risk scores using numerical formulas
2. Perform decision support using PELLET

### Recommended preoperative investigations

- Set of recommendations, investigations and factors influencing recommendations

- in practise health assessors have  difficulties in using the guidelines

- NICE guideline rules are translated into ABoxes and TBoxes

- Complex rules are simplified to remain interpretable

Using OWL Ontologies for clinical guidelines based comorbidity decision support
----

### Abstract

Paper presents ontology-based clinical decision-support framework for handling
comorbidities by alignment of ontology modelled CPGs.

### Preamble

Good quote for definition of CPGs.

### Methodological approach

Good quote for OWL definition


Clinical practise guidelines: a case study of combining OWL-S, OWL and SWRL
----

### Abstract

Paper used OWL-S (Ontology Web Language for Services) to specify semantic types of
input and output data of a Web Service.


OWL-S describes Web Services in way computer can interpretate it in an automated manner.
OWL-S description includes:
a) Ontological description of input required by the service
b) Output that the input provides
c) Precondition and postcondition of each invocation

Services  in OWL-S are described in OWL-S as following:

1) *Service profile*: Description of what service does
2) *Service process model*: Describe how to use services
3) *Service grounding*: Specifies details of how to access /invoke a service


Applying SPARQL-Based inference and ontologies for modelling and execution of CPGs
------

### Introduction

Representing and executing CPGs via OWL and SPARQL-based inference rules.
Advantage: Possible to express negations.

Example CPG: Arterial hyptertension

### Case study and requirements

List four requirements necessary for CPG execution

### Approach and methodology

Guideline represented with Tasks and Action connected with
Object properties of type 'HasNextStep' tree.

State of patient represented through property 'HasExecutionState'.
SPARQL Queries are updating the patient ontology. The query has the conditions
for either path.

Difference to own work: Time relation. Possibility of recommending multiple
measures and a therapy plan
