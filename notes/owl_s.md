Summarization of OWL-S: Semantic Markup for Web Services
===

Services on the Semantic Web
---

Services on the web are recognized as ones that do not only provide static information but allow
someone to make an action or change in the world. For instance, selling a product or control a
physical device.
To provide a Web services, software agents do need a computer-interpretable description of the
service and the means by which it is accessed. Semantic web markup languages try to establish
such a framework within these descriptions are made and shared.
As a consequence websites should employ ontologies for declaring and describing services.

OWL-S is a ontology which is able to solve those tasks.

Motivation
---

Two different kind of services are declared:

1. Primitive/Atomic: Services which are invoked by a request message and produce maybe a single
   response. There is no ongoing interaction between user and the service.
   For instance, a service which returns a postal code.

2. Complex/Composite: Services which are a combination of atomic services. May require user
   interaction. As a good example an interaction on `amazon.com` can be named. First searching for a
   book on various criteria, reading reviews, possible buy, entering payment information.

OWL-S should be able to fulfil three task types:

1. **Automatic Web service discovery**: An automated process for the discovery of a specific Web
   Service that can provide a particular class of service capabilities. For example, a user may
   search for a train ticket between two cities. Usually the user would need to search in a search
   engine. With OWL-S the information can be specified in a computer-interpretable semantic markup
   at the service Web site and ontology-enhanced search engine would locate all possible results.

2. **Automatic Web service invocation**: Describes a service which can be automatically invoked by
   an agent or computer program, given only a declarative description.

3. **Automatic Web service composition and interoperation**: "This task involves the automatic
   selection, composition, and interoperation of Web services to perform some complex task, given a
   high-level description of an objective."

An Upper Ontology for Services
---

Each distinct published service has an instance of Class **Service** which has three properties:
*describedBy*, *supports* and *presents*. The according ranges are:

1. Service Model (how it works): Tells client how to use the service, by detailing the semantic content of the
   requests, the conditions under which particular outcomes will occur, and, where necessary, the
   step by step processes leading to those outcomes.

2. Service grounding (how to access it): Specifies the details of how an agent can access a service. Usually a
   grounding will define a communication protocol, port number and so on. In addition a way of
   exchanging data elements for input and output must be specified

3. Service profile (what it does): Provides information which are needed to become discovered by an agent. The
   profile tells what it does in a suitable way.

Links
--
Martin, David, et al. "OWL-S: Semantic markup for web services." W3C member submission 22.4 (2004).

