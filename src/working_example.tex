\chapter{A Case Study}
\label{ch:working_example}

The preceding chapter has discussed all components that are necessary to create a treatment plan,
this one demonstrates the results on a sample use case.  The labels of the edges are still
based on the mapping displayed in \tableref{tab:mapping}.


\section{Diagnose Acute Schizophrenia}
\label{sec:step1}

It starts off with a middle-aged, female patient whose medical record (see \figureref{fig:cgm_example_1}) consists of three
different symptoms and a diagnose of acute schizophrenia. The corresponding treatment plan is
displayed in~\figureref{fig:working_example_1}.

The first thing to notice is the set of next recommendations. Those are a subset of recommendations
that are specified in the guideline in case acute schizophrenia is diagnosed. Since there are
many recommendations listed, only a subset has been added to the ontology; for example, a
diabetes screening or checking for suicidal risk. One of these recommendations worth
mentioning is breast cancer screening which is only suggested for women.
The patient has two conditional recommendations addressing possible existing comorbidities like
depression or initial dyskinesia. If one of these occurs while suffering from acute schizophrenia,
it is necessary to start another antidepressant therapy or application of a particular drug. In
order to cover these comorbidities in more detail, it is necessary to include the corresponding
guidelines, for example, the one for
depression\footnote{\url{http://www.bptk.de/uploads/media/20091202_depression_kurz.pdf}}. Since
this is a proof of concept, the implementation of the guideline for depression has not been covered
at all.

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.95\textwidth,trim=0.4cm 18.0cm 0.2cm 1cm, clip]{img/working_example/working_ex1.png}
	\end{center}
	\caption{Medical record for example patient - step 1.}
	\label{fig:cgm_example_1}
\end{figure}


The next essential recommendations concern the start of a pharmacotherapy. Hence, the patient should
be at first informed about the therapy and secondly approve to start it. Since the diagnosed
schizophrenia is not yet marked as being the first occurrence, the treatment plan covers both
options. At first, the question if it is the first manifestation of acute schizophrenia is raised.
In case the patient already has had acute schizophrenia, the treatment plan suggests to answer the
question whether there is still an ongoing application or not. In case it is the first time a
schizophrenia manifests, the application of a drug will be suggested next.

Additionally, the question if the patient has already a non-successful Clozapine application, is
already negated in the treatment plan. This decision is arguable due to the open world
assumption. However, we assumed that such an important event would be mentioned in the medical
record of the patient.

\section{Application of Atypical Antipsychotic Drug}
\label{sec:step2}

Next, \figureref{fig:cgm_example_2} shows the updated medical record; the patient has been
asked for approval of pharmacotherapy and has had a few measures like a blood count, MRT and CT.
Additionally, the drug Quetiapine in dosage $\SI{25}{\milli\gram}$ has been applied and the acute
schizophrenia has been marked as a first occurrence. Hence, the corresponding treatment plan
in \figureref{fig:working_example_2} is a bit smaller; it does not need to cover the option of
having acute schizophrenia for the second time. Furthermore, the condition that asks for
a change of the applied antipsychotic has one successor less compared to the former plan.
This is caused by the fact that the application of Quetiapine is recognised as a starting dose.
Therefore, in case of being evaluated as maintained and not successful, it is recommended to apply
the drug in target dose range only.

\begin{figure}
	\begin{center}
		\includegraphics[width=0.95\textwidth,trim=0.2cm 16.0cm 0.2cm 1cm, clip]{img/working_example/working_ex2.png}
	\end{center}
	\caption{Medical record for example patient - step 2.}
	\label{fig:cgm_example_2}
\end{figure}

\section{First Non-successful Application}
\label{sec:step3}

The application of Quetiapine to its starting dose has been stopped after
being considered as not successful but worth maintaining. The updated treatment plan is seen
in \figureref{fig:working_example_3}. The plan recommends the application of
Quetiapine in its target dose. Additionally, it has changed once more regarding the
continuation if the antipsychotic is marked as not successful and not changed. Subsequently, the dose of
Quetiapine should be decreased or increased slightly. With regard to the medical record, the patient has had a diabetes
screening, and the next one is recommended in about $296$ days. The temporal distance of
ten months is not hardcoded in the guideline, instead it is stated that diabetes screening needs to be done at least once
in a year. As a consequence, ten months were supposed as a good period of time to recommend it
again. The problem regarding imprecise specifications in the guideline is tackled in Chapter~\ref{ch:misc}.

\section{Diagnosing a Therapy Resistance}
\label{sec:step4}

Until now the  treatment plan has only changed a little bit after every step. This changes if a
significant incident happens, for example diagnosing a therapy resistance, as the resulting
treatment plan in \figureref{fig:working_example_4} shows. The patient has been diagnosed with a resistance to
the currently applied drug. Therefore, the immediate recommendations for the practitioner is to stop
the applied drug and mark the application as unsuccessful. The rest of the treatment plan is
relatively determined since the antipsychotic drug needs to be changed, the patient has had a therapy
resistance in their medical record, and this resistance occurred by the last applied drug. At this
point, it is worth to mention that cycles in the treatment plan cause problems. Since
after an atypical drug is applied as next, the treatment plan will have major changes as seen
in \sectionref{sec:step5}.  As a consequence, there is a trade-off in displaying a
treatment plan. For now, the treatment plans can be considered as correct until the occurrence of a cycle.

\section{Drug Application with Resistance in the Medical Record}
\label{sec:step5}

After the diagnosed therapy resistance, the application of Quetiapine has been cancelled. Furthermore,
the patient has started a cognitive behavioural therapy as recommended earlier (see
\figureref{fig:cgm_example_5} for the corresponding medical record).
As a consequence, the recommendation for cognitive behavioural therapy is no longer part of the treatment
plan. Since the patient has a therapy resistance in medical history, this information is provided
in the treatment plan. Because this resistance is not further related to the application of
Amisulpride, the treatment plan states that the patient does not have a resistance to the currently applied
drug.

Since the applied dose of Amisulpride is $\SI{200}{\milli\gram}$, it is considered as both starting
and targeting dose for the first manifestation. Therefore, the treatment plan recommends to
increase or decrease the dose in the associated dose range in case the application was unsuccessful.
\figureref{fig:working_example_5} displays the resulting treatment plan.


\begin{figure}
	\begin{center}
		\includegraphics[width=0.95\textwidth,trim=0.2cm 19.5cm 0.2cm 1cm, clip]{img/working_example/working_ex5.png}
	\end{center}
	\caption{Medical record for example patient - step 5.}
	\label{fig:cgm_example_5}
\end{figure}


% Diagnose of acute schizophrenia
\begin{sidewaysfigure}[ht]
	\includegraphics[width=\textwidth, height=12cm]{img/working_example/Working_Example1}
	\caption[Treatment plan worked example 1]{Treatment plan for a patient that got diagnosed with acute schizophrenia.}
	\label{fig:working_example_1}
\end{sidewaysfigure}

% First application
\begin{sidewaysfigure}[ht]
	\includegraphics[width=\textwidth, height=10cm]{img/working_example/Working_Example2}
	\caption[Treatment plan worked example 2]{Treatment plan for a patient that has started an antipsychotic therapy and got its
	first application of Quetiapine.}
	\label{fig:working_example_2}
\end{sidewaysfigure}


% First application marked as not successful but maintained
\begin{sidewaysfigure}[ht]
	\includegraphics[width=\textwidth, height=12cm]{img/working_example/Working_Example3}
    \caption[Treatment plan worked example 3]{Treatment plan for a patient whose first application is marked as not successful and
	needs to changed.}
	\label{fig:working_example_3}
\end{sidewaysfigure}


% Second application with drug resistance
\begin{sidewaysfigure}[ht]
	\includegraphics[width=\textwidth, height=6cm]{img/working_example/Working_Example4_unf}
    \caption[Treatment plan worked example 4]{Treatment plan that represents a patient having a therapy resistance.}
	\label{fig:working_example_4}
\end{sidewaysfigure}

% After drug resistance
\begin{sidewaysfigure}[ht]
	\includegraphics[width=\textwidth, height=10cm]{img/working_example/Working_Example5}
    \caption[Treatment plan worked example 5]{Treatment plan for a patient with its second drug application and a therapy resistance in the
	medical record.}
	\label{fig:working_example_5}
\end{sidewaysfigure}
