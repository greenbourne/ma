\chapter{Miscellaneous}
\label{ch:misc}

This chapter briefly describes a few results which are not directly related to the topic of the
thesis but are worth mentioning.

\subsubsection{Duration of Reasoning}
\label{subsubsec:duration}

While the ontology has been extended continuously, at the same time the duration of reasoning
has been increased.
The first spike during the reasoning process occurred after introducing transitive object properties and
ended up taking about $\mathsf{10}$ minutes at the peak. It turned out that the time
decreases rapidly once disjoint axioms are introduced whenever possible. As a result, the reasoning
time has been reduced to half. Thus, high reasoning times can be an
indicator for a lack of disjoint axioms and on the other hand, it could be handled by minimisation
of the use of transitive object properties.

\subsubsection{Bug in Openllet and Pellet}
\label{subsubsec:bugs}

After introduction of application of drugs (see Section~\ref{subsec:drugs}), it was
necessary to distinguish the different applications not only with regards to the applied drug, but also by the
dosage rate. A class for each drug combined with its dosage rate has been introduced (see~\figureref{fig:first_manifest}).

\begin{wrapfigure}{i}{0.6\textwidth}
	\begin{center}
		\includegraphics[scale=0.7]{img/miscellaneous/first_manifestation_dosage.png}
	\end{center}
	\caption[Targeting dose classes]{Measures that represent the application of a drug at a target dose level while having
	the first manifestation of acute schizophrenia.}
	\label{fig:first_manifest}
\end{wrapfigure}

Each class covers a specific dosage range according to the guideline. For example, the target dose of
Amisulpride for each day lies between $\SI{200}{\milli\gram}$ and  $\SI{300}{\milli\gram}$.
It turned out that both reasoners, Pellet and Openllet, had a bug in their backtracking algorithm
when dealing with classes which have datatype properties with overlapping ranges. For instance, the two
classes mentioned in~\listingref{lst:bug} caused an internal reasoner exception\footnote{See
\url{https://github.com/Galigator/openllet/issues/32} for viewing the full bug report.}

The fact that such a bug has not been discovered yet might be connected to the fact that the last
release of the Pellet reasoner was in
$\mathsf{2011}$\footnote{\url{http://semanticweb.org/wiki/Pellet}} and only one person maintains the
fork Openllet. Hence, it is strongly recommended to use at least version 2.6.4 of Openllet when
using the CGM/F20 framework.


\begin{minipage}{\linewidth}
	\begin{lstlisting}[language=xml, label=lst:bug, caption={[Bugged OWL classes]Two example classes with overlapping
		data-type ranges causing an internal reasoner exception}]
Class: TargetDose_FirstManifestation_Olanzapin

    EquivalentTo:
        (concreteDrug some Olanzapin)
         and (dose some xsd:decimal[>= 5 , <= 10])


Class: TargetDose_MultipleManifestation_Olanzapin

    EquivalentTo:
        (concreteDrug some Olanzapin)
         and (dose some xsd:decimal[>= 5 , <= 20])
     \end{lstlisting}
\end{minipage}

\subsubsection{Pre-existing Medical Conditions}
\label{subsubsec:pre}

The schizophrenia guideline points out that patients are treated differently depending on their pre-existing conditions,
gender and age. To this end, the CGM/F20 framework has been extended by a GUI
which is invoked whenever a new record for a patient is created (see ~\figureref{fig:precond}).
On an ontological level, the age is saved through a data property while gender and preconditions are
represented by making the patient a member of equivalent classes.

\begin{figure}[hb]
	\begin{center}
		\includegraphics[scale=0.7]{img/miscellaneous/precondition.png}
	\end{center}
	\caption[CGM GUI for new patient]{GUI for inserting information when adding a new patient.}
	\label{fig:precond}
\end{figure}

\subsubsection{Translation Challenges}
\label{subsubsec:trans_chal}

As it is often the case, guidelines tend to be vague in their formulation. For instance, the
guidelines suggest application of breast cancer screening periodically (see bullet point
number $162$ in the schizophrenia guideline~\cite{dgppn:125}). It is not further specified what
'periodically' means.
As another example serves the recommendation given by bullet point $21$; the change of a drug is
discussed in case of 'deficient efficiency'.  There exists no further parameter when  applications
should be classified as deficient. These examples point to the fact that many situations during medical
treatment must be judged individually by the attending physicians in cooperation with their patient.
