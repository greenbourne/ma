\chapter{Introduction}
\label{ch:introduction}
\section{Motivation}
\label{sec:motivation}
% Origins and advantages of CPGs
Clinical practice guidelines (CPGs) are well established for many
decades. Their roots lie in the US health care system, based on the idea
to apply clinical results from evidence-based medicine into clinical practice~\cite{iom92}.
CPGs result from a long and intense procedure initiated mostly by national
bodies, which eventually see demand for a new guideline.
There are several steps for publishing a guideline, they are: Generate an evidence-based recommendation, ratify it,
formulate it as CPG and finally let it be reviewed by autonomous medical experts~\cite{browman1995practice}.

As many studies confirm, the upside of CPGs resides in their capacity to improve healthcare of
patients~\cite{fritz2007does}; for instance through a reduction of medical
errors~\cite{chen2010guideline}, a better cost-efficiency~\cite{cromwell1997cost}, and standardised
workflows~\cite{eagle2005guideline}.

% paper showing high costs and gap between evidence based medicine and practice
However, research results are showing that there exists a considerable gap between results of
evidence-based medicine and the use of it in practice. The so-called dissemination process of CPGs
is often imperfect and one of the reasons why patients do not receive the best possible care.
Barriers to integrate guidelines in practice have been identified as a combination of deficits in
organisational (e.g., lack of time), social (e.g., social routines, disagreements with colleagues)
and professional context (e.g., information overload, the complexity of
guidelines)~\cite{boxwala2001toward,davis1997translating,gro03}.  Thus, the
adherence of guidelines is of great interest in active research~\cite{LugtenbergEA11}.

% CDS systems explanation
To deal with this challenge, clinical decision support (CDS) systems are recognised as one of the best
strategies. These systems provide a reliable resource for practitioners, patients, and other staff members
as they comprehend and filter relevant information for them. Furthermore,
they can be displayed in a natural, understandable manner and recommend next steps in the current therapy.
Therefore CDS systems are a way to make clinical knowledge available for
all, while making financial sense to the end-user and allowing a steady improvement through feedback and
new findings in research~\cite{osheroff2007roadmap}.

% OWL systems
Recent research is increasingly focussing on knowledge-based approaches towards
CDS systems.
Especially languages like OWL are well suitable for this use-case because they are both, machine and
human readable as well as able to store complex information with the possibility to infer new
knowledge by combining it with a reasoning tool as seen in example \cite{BouamraneEA09}.

% End introduction & Introduce my work
In this thesis, we present a proof-of-concept for such a CDS system which does not only
recommend diagnosis and measure also creates a complete treatment plan for patients,
depending on their current state.

\section{Related Work}
\label{sec:related}
% Paper using OWL to formalise guidelines
There already exists work that focuses on ontology developed decision support.
Bouamrane, Rector, and Hurrell~\cite{BouamraneEA09} presented a system for preoperative risk
assessment, where the guideline NICE, which belongs to the activity field of preoperative risk, has
been translated into OWL as well as the patient medical history. The decision support is afterwards
organised in two subsequent steps. First, risk scores are derived and calculated with mathematical
formulas; second, the actual decision support is performed using a reasoner. This concept overlaps
to a great extent with the basic approach in our work. In contrast to their work, the main
conceptual differences are that we introduce a temporal concept for events and use queries for
interaction with the ontologies.

In the same direction heads the work of Doulaverakis et al.~\cite{doulaverakis2016applying}.
Their ontology, which represents the CPG for hypertension management, includes classes for
patients and tasks that are potentially connected through a \textit{hasNextStep}
object property.

\begin{minipage}{\linewidth}
	\begin{lstlisting}[language=sql,keywords={DELETE,INSERT,WHERE,FILTER,NOT,
	EXISTS},label=lst:state_transition, caption={[State transition]A query that performs a state
		transition for a patient.}]
		# State transition from A to B
		DELETE {?gl hasExecutionState checkForDiabetesOrCKD}
		INSERT {?gl hasExecutionState checkAge 60}
		WHERE {
			?gl hasExecutionState checkForDiabetes.
			FILTER NOT EXISTS {
				?patient hasDisease ?diasease
				FILTER (?diasease=Diabetes || ?disease=CKD)
			}}
	\end{lstlisting}
\end{minipage}

A section of the algorithm for Management of Arterial Hypertension is displayed in
Figure~\ref{fig:arterial} to illustrate that each decision or task is considered as a unique state
for the patient. Therefore each patient can be connected to an object property
\textit{hasExecutionState}. The application of state changes are made through SPARQL queries (see
Listing~\ref{lst:state_transition}). The filter statement is used to apply changes only for patients
that fulfil a set of conditions. Our work
distinguishes from such a hard-wired approach like this because of their reluctance to entail knowledge.

\begin{figure}
	\begin{center}
		\includegraphics[width=0.35\textwidth]{img/introduction/arterial_hypertension_flow}
		\caption{A section of Arterial Hypertension CPG flow graph}
		\label{fig:arterial}
	\end{center}
\end{figure}


% OWL -S
Another option for modelling clinical guidelines is the usage of the \textit{Ontology Web Language for
Services (OWL-S)}~\cite{martin2004owl}. OWL-S is based on the idea of providing a semantic
markup language framework within which services on the web are described and shared.
Each service is characterised by three properties. They are:
\begin{enumerate}
	\item \textbf{Service Model}: A semantic description for clients on how to use the service
		and how it works.
		Therefore the semantic content of the requests is described with all possible outcomes.
	\item \textbf{Service Profile}: Provides information agents need to figure out what the
		service does.
	\item \textbf{Service grounding}: Describes how the service can be accessed by defining
		information like a communication protocol and port number.
\end{enumerate}

OWL-S declares two different types of services. On the one hand, \textit{Atomic Services}, invoked
by a request message, produce a single response without further interaction between the user and
service.  On the other hand, \textit{Composite Services} are present, and they are a
combination of atomic services, which require user interaction.

Argüello and Des~\cite{casteleiro2008clinical} have created  three composite
web services with the help of OWL-S to construct a service-based application for clinical management. One
service gathers information about a patient, another provides functionality to find a
relevant clinical guideline, and the last one provides recommendations based on the available
evidence. These three processes can be combined to represent a medical workflow. In
Chapter~\ref{ch:future} an alternative approach for our work, which considers OWL-S, will be discussed in detail.

% Reference to basic work
This thesis is based on a previous work by Gorin et al.~\cite{gorin2017ontological}
who built a proof of concept \textit{Clinical Guideline Module} (CGM) that uses the
patients \textit{Electronic Health Records} (EHRs) to assist practitioners with decision support.
It consists of two parts, a generic clinical guideline module and formalisation of parts of the
\textit{Schizophrenia} guideline of the German Association of Psychiatry, Psychotherapy and
Psychosomatics (DGPPN)~\cite{dgppn:125}, which is currently instantiated to ICD10 diagnostic code
F20. That is why the tool is called CGM/F20.

The work tackles several implementation challenges. To name a few:
\begin{enumerate}
	\item Temporal relations: An ontology for time has been introduced since it is crucial to indicate
		temporal order of events like diagnosis or measures.
	\item Partial knowledge: The system follows the \textit{open world assumption}, which means that
		the absence of information does not lead to the conclusion that it does not exist.
\end{enumerate}

Our thesis extends CGM/F20 and implements, as mentioned in the 'Conclusions and Future Work'
section, by a lookahead function that reorganises clinical paths as the
clinical process unfolds.

\section{Contribution of the Thesis}
\label{sec:contribution}

Our work contributes a proof of concept CDS system which expands the tool CGM/F20 (see
~\cite{gorin2017ontological}) to the possibility of inferring complete treatment plans for patients based on
their status and clinical background. This approach is contrary to conventional ones of hard wiring
guideline plans in flow-based diagrams as it allows for a dynamic replanning of clinical
pathways, while the actual clinical process unfolds.  We discuss in this work the way of constructing a
treatment plan for patients with foresight and presents results in forms of derived treatment plans
for generic patients. The code is open source and available at
\url{https://www8.cs.fau.de/research/cgm}\footnote{Use \texttt{git clone} \url{git://git8.cs.fau.de/CGM} to
get read access.}.

In Chapter~\ref{ch:tool_environment} a short introduction to OWL and SPARQL will be given, followed
by the implementation process in Chapter~\ref{ch:implementation}. Afterwards,
Chapter~\ref{ch:working_example} will show the results by providing a case study. In the end,
Chapter~\ref{ch:misc} addresses some issues which are partially related to the results of our work
and worth mentioning. Finally, Chapter~\ref{ch:future} shares some ideas for future work.
