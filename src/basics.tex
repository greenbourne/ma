\chapter{Tool Environment}
\label{ch:tool_environment}

\section{Ontological Basics}
\label{sec:ontological_basic}
Ontologies are 'a description of knowledge about a domain of
interest'~\cite{hitzler2009foundations} with a processable machine specification. This
chapter will give a short introduction to the most used ontology language
OWL~\cite{OWL} (see Section~\ref{subsec:owldl}) with its logical background (see
Section~\ref{subsec:dec}), to the SPARQL query language (see
Section~\ref{subsec:sparql}) and SWRL rules (see \sectionref{subsec:swrl}). Afterwards, Section~\ref{sec:cgm}
summarises the main features of CGM/F20. At last all relevant tools are listed in Section~\ref{sec:tools}.

\subsection{Web Ontology Language (OWL)}
\label{subsec:owldl}

The \emph{World Wide Web Consortium (W3C)} developed the \emph{Web Ontology Language} OWL and
standardised it in the year 2004. OWL 2~\cite{OWL2}, standardised in year 2011, fixed several
shortcomings 'such as expressivity issues, problems with its syntaxes, and deficiencies in the
definition of OWL species'~\cite{grau2008owl}.

Our focus of this chapter describes the main features of OWL.  There are many different formats
available for representing OWL ontologies, for example, RDF/XML~\cite{RDF}, Manchester OWL
Syntax~\cite{MANCHESTER} and Turtle~\cite{TURTLE} - to name the most popular ones. Throughout this
thesis the Manchester OWL Syntax is used for code fragments despite the fact that it has a lower
expressiveness than RDF/XML (for the benefit of readability).

Before presenting the basic blocks of an ontology, it is worth mentioning that the following examples
are part of the implemented ontologies. On the one hand, the \emph{time} ontology takes care of all
time-related parts, and on the other hand each information that is directly related to the schizophrenia guideline
is part of the \emph{schizophrenia} ontology. Therefore classes and properties have prefixes
marking to which ontology they belong.  The \texttt{time:} prefix signalises that the
corresponding property is part of the \emph{time} ontology, while the empty prefix \texttt{:} always refers
to the \emph{schizophrenia} ontology.

The basic blocks of OWL are roles, or rather properties, classes and individuals. OWL classes represent sets of
entities in the domain of interest. They can be put in relationship with other classes via
subclass statements as shown in~\listingref{lst:owl_class}.

\begin{minipage}{\linewidth}
	\begin{lstlisting}[basicstyle=\small, language=xml, label=lst:owl_class, caption={[Manchester
		syntax]A simple class written in Manchester OWL Syntax.}]
	Class: :Patient
	
	    SubClassOf:
	        :Human.
	\end{lstlisting}
\end{minipage}

One can define individuals and assign them to classes as done in the
exemplary~\listingref{lst:owl_ind}. An individual named \texttt{patient1} is assigned to the
classes \texttt{Patient} and \texttt{Female}.

\begin{minipage}{\linewidth}
	\begin{lstlisting}[basicstyle=\small, language=xml, label=lst:owl_ind, caption={[Example
		patient]An exemplary individual that is member of the classes \texttt{Female} and
		\texttt{Patient}.}]
	Individual: patient1
	
	    Types:
	        :Female,
	        :Patient.
	\end{lstlisting}
\end{minipage}

Properties are used to relate two individuals.  Each property may
specify a domain and range to record that the individuals are necessarily members of certain
classes. Besides, it is possible to set properties in relation to others by defining
sub-properties or declaring them as the inverse to each other (see the following
Listing~\ref{lst:owl_role}).

\begin{minipage}{\linewidth}
	\begin{lstlisting}[basicstyle=\small, language=xml, label=lst:owl_role, caption={[Object
		property definition]An object property definition
		that relates an individual of class \texttt{Patient} with one of class \texttt{Diagnosis}.}]
	ObjectProperty: :hasDiagnosis
	
	    SubPropertyOf: 
	        :hasEvent
	
	    Domain: 
	        :Patient
	
	    Range: 
	        :Diagnosis
	
	    InverseOf: 
	        :hasDiagnosisInverse
	\end{lstlisting}
\end{minipage}

% Role restrictions (page 126)
The definition of more complex classes involves properties which requires \emph{role restrictions}.
These role restrictions are closely related to the quantifiers in predicate logic which are reflected by the
keywords: \texttt{only} and \texttt{some}\footnote{Respectively \texttt{owl:allValuesFrom} and
\texttt{owl:someValuesFrom} in RDF/XML syntax}. With them, it is possible to define a complex class
like in \listingref{lst:owl_role_restriction}, which represents individuals that have at least one
current diagnosis of acute schizophrenia. For the special case that the connection to a specific
individual needs to be expressed, the keyword \texttt{value} will be used.

\begin{minipage}{\linewidth}
	\begin{lstlisting}[language=xml, label=lst:owl_role_restriction, caption={[Existential
		quantifier in ontologies]Using an existential quantifier to define a class of patients that have at least
		one ongoing diagnosis of acute schizophrenia.}]
	Class: :PatientsWithOngoingAcuteSchizophrenia
	
	    EquivalentTo:
	        :hasDiagnosis some
	            (:AcuteSchizophrenia
	             and time:CurrentEvent)
	\end{lstlisting}
\end{minipage}



% OWL-DL restriction towards cardinality (page 143)

Furthermore, OWL allows a more fine-grained usage of quantifiers. Through the cardinality support
a minimum or maximum amount of properties needs to exist. For instance,
the class in \listingref{lst:owl_role_card} defines patients who have had at least two active symptoms
of a specific type for at least a period of $28$ days.

\begin{minipage}{\linewidth}
	\begin{lstlisting}[language=xml, label=lst:owl_role_card, caption={[Cardinality in roles]An
		exemplary cardinality role definition
		which relates individuals of the classes \texttt{Patient} and \texttt{Diagnosis}.}]
Class: :RecommendDiagnosisAcuteSchizophrenia_Rule_2

    EquivalentTo:
        :hasSymptom min 2 (:Symptom1to4
         and time:CurrentEvent
         and (time:hasDuration some xsd:int[>= "28"^^xsd:int]))

    SubClassOf:
        :RecommendDiagnosisAcuteSchizophrenia
	\end{lstlisting}
\end{minipage}

\subsection{The Logical Background of OWL}
\label{subsec:dec}
OWL 1 provides three different sublanguages: OWL FULL, OWL DL and OWL Lite;
OWL DL is a subset of OWL FULL and OWL Lite is a subset of OWL DL. For OWL 2 sublanguages OWL 2 FULL
and OWL 2 DL exist as well. Additionally three other sublanguages, which are a subset of OWL
2 DL, have been introduced:
OWL 2 EL, OWL 2 QL and OWL 2 RL. Apart from the FULL variants, each of them are strongly related to
a \emph{description logic} (DL). DLs are subsets of \emph{first order logic} (FOL) and are usually
decidable. Moreover, efficient algorithms for reasoning exist.
The description logic for OWL 1 DL is called $\mathcal{SHOIN}(D)$, while the one for OWL 2 DL is
called $\mathcal{SROIQ}(D)$; both are decidable~\cite{horrocks2006even,HorrocksEA03-SHORT} and base
on the description logic $\mathcal{ALC}$.
At first, the syntax and semantics of $\mathcal{ALC}$ will be explained. The formal syntax for class
expressions looks as follows:
\begin{align*}
  C,D ::= \bot \mid A \mid \neg C \mid C \sqcap D \mid C \sqcup D\mid \exists R\ldotp C && (A\in N_C, R\in N_R)
\end{align*}
$N_C$ defines a set of class-names and $N_R$ a set of roles-names. In $\mathcal{ALC}$ two types
of statements or axioms exist:
\begin{enumerate}
	\item ABox: An axiom of form $C(a)$ or $R(a,b)$ where $a,b$ are individuals, $C \in N_C$ and $R
		\in N_R$. Its purpose is to state facts about individuals.
	\item TBox: An axiom of form $C \equiv D$ (equality) or $C \sqsubseteq D$ (general class
		inclusion). It either states the equality of two class expressions or that one class
		is a subset of another one.
\end{enumerate}

Many OWL (2) DL language constructs already can be expressed in $\mathcal{ALC}$, for example:

\begin{minipage}{\linewidth}
	\begin{lstlisting}[basicstyle=\small, language=xml, label=lst:alc, caption={[Example statment in $\mathcal{ALC}$]Some exemplary
		statements expressed in $\mathcal{ALC}$.}]
		Patient(patient1)
		PatientsWithOngoingAcuteSchizophrenia$\equiv$
			$\exists$ hasDiagnosis.  $($ AcuteSchizophrenia $\sqcap$ CurrentEvent $)$
	\end{lstlisting}
\end{minipage}

% Interpretation
For the semantics of $\mathcal{ALC}$, an interpretation function
$\cdot^\mathsf{I}$ is defined and it consists of the following components:

\begin{enumerate}
	\item A set of entities called \emph{domain of interpretation}, which is denoted by
		$\Delta^\mathsf{I}$.
	\item A mapping for all classes (individuals are formally viewed as a special case of
		classes): $A \in N_C: A^\mathsf{I} \subseteq \Delta^\mathsf{I}$
	\item A mapping for all roles:   $R \in N_R: R^\mathsf{I} \subseteq \Delta^\mathsf{I} \times \Delta^\mathsf{I}$
\end{enumerate}


Next, the \emph{direct semantics} extends $\cdot^\mathsf{I}$ and lifts the interpretation of
individuals, class names, and role names to complex classes and roles expressions.

\textbf{Direct Semantics for $\mathcal{ALC}$:}

\begin{itemize}
	\item $\bot^\mathsf{I} = \emptyset$; $\top^\mathsf{I} = \Delta^\mathsf{I}$
	\item Describe all things that are not in $C$:\\ $\left(\neg C\right)^\mathsf{I} = \Delta^\mathsf{I} \backslash C^\mathsf{I}$
	\item Describe all things that are in $C$ and $D$, respectively in $C$ or $D$:\\$\left(C \sqcap D\right)^\mathsf{I} = C^\mathsf{I} \cap D^\mathsf{I}$; $\left(C \sqcup D\right)^\mathsf{I} = C^\mathsf{I} \cup D^\mathsf{I}$
	\item Describe those things that are connected via $R$ to something in $C$:\\$\left(\exists R.C\right)^\mathsf{I} = \{ \mathsf{d} \in \Delta^\mathsf{I} | \exists
		\mathsf{e} \in \Delta^\mathsf{I} . \left(\mathsf{d,e}\right) \in R^\mathsf{I} \wedge
		\mathsf{e} \in C^\mathsf{I}\}$
\end{itemize}

However, $\mathcal{ALC}$ is not as expressive as required for OWL (2) DL since features like \emph{inverse roles}
or \emph{role transitivity} are missing. As already mentioned, different variants of
description logic exists. Their terminology is systematic, where each letter indicates another
feature. The meaning of the letters are listed below:
\begin{itemize}
	\item [$\mathcal{S}$] stands for $\mathcal{ALC}$ plus role transitivity.
	\item[$\mathcal{R}$] stands for role functionality.
	\item[$\mathcal{O}$] stands for nominals, for example for closed classes with one element.
	\item[$\mathcal{I}$] stands for inverse roles.
	\item[$\mathcal{N}$] stands for cardinality restrictions.
	\item[$\mathcal{Q}$] stands for qualified cardinality restrictions.
	\item[$\mathcal{H}$] stands for role hierarchies, e.g. role inclusion axioms.
	\item[$D$] stands for datatypes.
\end{itemize}

This explains the meaning of $\mathcal{SHOIN}(D)$ and $\mathcal{SROIQ}(D)$. Next, the syntax and
semantics of $\mathcal{SROIQ}(D)$ will be described, since our ontologies have this expressivity.
The formal syntax for class expression in $\mathcal{SROIQ}(D)$ has the following form:
\begin{align*}
	C,D ::= \bot \mid A \mid \neg C \mid C \sqcap D \mid C \sqcup D\mid \{a\} \mid \exists R\ldotp C \mid
	\forall R\ldotp C \mid \exists S\ldotp \mathsf{Self} \mid \leq nS\ldotp C \mid \geq nS\ldotp C
	\\ (A\in N_C, R,S\in N_R, a\in N_I, n\in\mathbb{N}^+)
\end{align*}

$N_I$ defines a set of individuals and each individual is an instance of $\exists
S\ldotp\mathsf{Self}$ if it is related to itself via the role $S$. Besides TBox and ABox, which are
nearly identical with the definition above (ABoxes may contain negated roles as well), in $\mathcal{SROIQ}(D)$ another statement type exists:
\begin{enumerate}
	\item RBox: An axiom to state facts about roles. It is of the form $R \equiv S$ (equality of
		roles), $R \sqsubseteq S$ (\emph{generalised role inclusion}) and $R_1 \circ R_2 \sqsubseteq
		S$ (\emph{complex role inclusion}), where $R,S \in N_R$.
\end{enumerate}

% Interpretation
We now define the formal semantics of $\mathcal{SROIQ}(D)$. It uses the same interpretation function
as above with these additions:

\textbf{Direct Semantics for $\mathcal{SROIQ}(D)$:}
% $C^\mathsf{I} \subseteq \Delta^\mathsf{I}$ (written as $\mathsf{I}, \mathsf{d} \models C \Leftrightarrow \mathsf{d} \in C)$:

\begin{itemize}
	\item Describe those things in $\mathsf{d}$ for which every $\mathsf{e}$, that connects from
		$\mathsf{d}$ via a role $R$, is in the class $C$:\\$\left(\forall R.C\right)^\mathsf{I} = \{ \mathsf{d} \in \Delta^\mathsf{I} | \forall
		\mathsf{e} \in \Delta^\mathsf{I}. \left(\mathsf{d,e}\right) \in R^\mathsf{I} \implies  \mathsf{e} \in C^\mathsf{I}\}$
	\item Describe those things that are connected via $R$ at most $\mathsf{n}$ times:\\$(\leq
		\mathsf{n}R.C)^\mathsf{I} = \{\mathsf{d} \in \Delta^\mathsf{I} | \#\{\mathsf{e} \in
		C^\mathsf{I} | \left(\mathsf{d},\mathsf{e}\right) \in R^\mathsf{I}\} \leq \mathsf{n} \}$
	\item $(\geq \mathsf{n}R.C)^\mathsf{I}$ analogous.
	\item $\{\mathsf{a}\}$ describes a class containing only $\mathsf{a}$:
		$\{\mathsf{a}\}^\mathsf{I} =  \{\mathsf{a}^\mathsf{I}\}$
	\item $\exists \mathsf{S}.\mathsf{Self}$ describes those things which are connected to
		themselves via $\mathsf{S}$:\\ $\left(\exists \mathsf{S} . \mathsf{Self}\right)^\mathsf{I} =
		\{ \mathsf{d} \in \Delta^\mathsf{I} | \left(\mathsf{d}, \mathsf{d}\right) \in
		\mathsf{S}^\mathsf{I}\}$
	\item Define an inverse role: $\left(R^{-}\right)^\mathsf{I} =
		\{\left(\mathsf{d},\mathsf{e}\right) | \left(\mathsf{e},\mathsf{d}\right) \in R^\mathsf{I}\}$
	\item Define the universal role $\mathsf{U}$: $\mathsf{U}^\mathsf{I} = \Delta^\mathsf{I}
		\times \Delta^\mathsf{I}$
\end{itemize}



For further information, Chapter $5$ of the book 'Foundations of Semantic Web
Technologies'~\cite{hitzler2009foundations} is a strongly recommended source.

\subsection{The Query Language SPARQL}
\label{subsec:sparql}
SPARQL~\cite{SPARQL} is a query language constructed for RDF graphs. The language RDF is considered
as OWLs predecessor and is 'based on a very simple graph-oriented data
schema'~\cite{hitzler2009foundations}. The syntax is characterised by triples consisting of
\emph{subject}, \emph{predicate} and \emph{object}, the subject represents a start node,
the predicate an edge and the object a target node. Through this serialisation to triples, a graph
can be adequately described by its edges.  Given any knowledge base in OWL, subjects and objects can be
considered as individuals and a predicate as a role that relates both.

The style of SPARQL is closely related to SQL due to its similar syntax and naming conventions.
For instance, in \listingref{lst:sparql_example} a query is formulated that returns all
individuals which are related via the role \texttt{hasDiagnosis} to an individual with a membership of \texttt{AcuteSchizophrenia}.

\begin{minipage}{\linewidth}
	\begin{lstlisting}[basicstyle=\small, language=sql, label=lst:sparql_example, caption={[SPARQL query for recommendations]A SPARQL
		query asking for patients having a diagnosis of acute schizophrenia.}, morekeywords={PREFIX}]
		PREFIX :<http://www8.cs.fau.de/research:cgm/schizophrenia#>
		SELECT ?patient
		WHERE{
		    ?patient :hasDiagnosis :AcuteSchizophrenia.
		}
	\end{lstlisting}
\end{minipage}
It is worth mentioning that \texttt{PREFIX} is a keyword which is used to declare shortcuts for
ontologies while \texttt{SELECT} defines the result format using variables (preceded by a question
mark) or any identifiers.  The keyword WHERE on the other hand initiates the actual query with a
set of triple graph patterns as described earlier.

SPARQL queries especially come in handy whenever a \emph{closed world assumption} is necessary.
The \emph{filter} statements allow to withdraw results which do or do not fulfil specific conditions.
Regarding our work, the main purpose for using filters is to assure that only events are
recommended that are not connected via a 'hasNonRecommendedEvent' property as well.
The query for finding recommended events is shown below in \listingref{lst:sparql_closed}.


\begin{minipage}{\linewidth}
	\begin{lstlisting}[basicstyle=\small, language=sql, label=lst:sparql_closed, caption={[SPARQL
		query using filters]A SPARQL query that asks for recommended events for a patient and filters out those
		that are not recommended.}, morekeywords={PREFIX, FILTER}]
    PREFIX :<http://www8.cs.fau.de/research:cgm/schizophrenia#>
    SELECT ?treatment
    WHERE{
        ?patient :hasRecommendedEvent ?treatment.
        FILTER NOT EXISTS {?patient :hasNonRecommendedEvent ?treatment}.
    }
	\end{lstlisting}
\end{minipage}

\subsection{The Semantic Web Rule Language (SWRL)}
\label{subsec:swrl}

The Semantic Web Rule Language extends the set of OWL axioms by Horn-like
rules~\cite{horrocks2004swrl}. They are structured as an implication as is shown
in~\figureref{fig:swrl}. A rule consists of an antecedent and a consequent, both are
a conjunction of atoms. Atoms have the form $\mathsf{C(x)}$, $\mathsf{P(x,y)}$,
$\mathsf{sameAs(x,y)}$, $\mathsf{differentFrom(x,y)}$ and a set of built-in functions where the
unary predicate $\mathsf{C}$ represents OWL classes, the binary predicate $\mathsf{P}$ OWL property
and $\mathsf{x,y}$ are either variables or individuals.
%Rules can only infer knowledge for
%existing individuals since otherwise OWL DL becomes undecidable~\cite{schmidt1989subsumption}.

\begin{figure}[hb]
	\begin{align*}
		\underbrace{\text{Body}}_{\textbf{antecedent}}& \to
		\underbrace{\text{Head}}_{\textbf{consequent}}\\
	\end{align*}
	\caption{The syntax of SWRL rules.}
	\label{fig:swrl}
\end{figure}

SWRL and OWL (2) DL have different expressive power although both are sublanguages of FOL. For
instance, SWRL rules are predestined for inferring data and object properties between individuals as
is shown in~\listingref{lst:swrl_time}, each event with a 'begin' and 'end' property is
related to a data property which represents its duration. It is worth mentioning that this rule
makes use of the built-in function \texttt{swrlb:subtract} which is satisfied if the first argument
equals the difference of the second argument minus the third one. In OWL, on the other hand, it is not
possible to use functions like \texttt{subtract} since it can only make assumptions about a set of
individuals in general. Moreover, SWRL is able to imply properties for individuals by using specific
knowledge like values of data properties.

\begin{minipage}{\linewidth}
	\begin{lstlisting}[basicstyle=\small, label=lst:swrl_time, caption={[SWRL rule for duration of events]A SWRL rule which infers
		a data property which represents the duration of events.}]
	time:hasBeginning(?ev, ?beg) $\wedge$ time:hasEnd(?ev, ?end)
		    $\wedge$ swrlb:subtract(?dur, ?end, ?beg)
				$\to$ time:hasDuration(?ev, ?dur)
	\end{lstlisting}
\end{minipage}

\section{An Overview of CGM/F20}
\label{sec:cgm}

CGM/F20 is capable of recording relevant medical data about patients and suggest further treatments
based on the corresponding guideline.

Medical patient data is understood as symptoms, (suggested) diagnosis and applied measures (see
\figureref{fig:history} for an example of a medical history). All events have a start and
optionally an end date and, consequently, a duration time. The tool distinguishes between
\emph{recent} and \emph{past} medical history, which relates to the fact that each event is
associated with a specific therapy session. Events that belong to the current session are listed
under \emph{Recent history} while all the others are listed in the \emph{Past history} tab.

\begin{figure}
  \includegraphics[width=\textwidth,trim=0.1cm 5.0cm 0.0cm 0.9cm, clip]{img/basics/cgm_recent_history.png}
  \caption{Medical history of a patient.}
  \label{fig:history}
\end{figure}

Under the \emph{Guideline assistant} tab, all current recommendations are listed (see
\listingref{fig:recommended}). The successful reasoning of the ontologies followed by a SPARQL query
are the steps upon which the evaluation is made (see Chapter $4$
in~\cite{gorin2017ontological} for more information).


\begin{figure}
  \includegraphics[width=\textwidth,trim=0.3cm 17.0cm 0.2cm 1cm, clip]{img/basics/cgm_recommended.png}
  \caption{Recommendations for a patient.}
  \label{fig:recommended}
\end{figure}

Regarding the recommendations, it is essential to understand how they are inferred.
They are based on the state of patients and their medical history and not any recommendations
that were made in an earlier state. Conversely, the state of a patient needs to be unique enough
to determine the correct set of recommendations.

% Explain recommendation individuals?
Another detail that needs to be mentioned is that SPARQL queries rely heavily on individuals. In
practice, this means that it is necessary to create a so-called dummy individual for each class that
represents an event like a measure, diagnosis or symptom. Their naming scheme is
\texttt{R\_Eventname} where the prefix '\texttt{R}' corresponds to 'Recommendation'. For SPARQL
queries it is not sufficient to specify implications by using existential quantifiers only, as it is
done in the \texttt{SubClassOf} section of \listingref{lst:insuf_class}. The reason for this is that
the SPARQL language is only capable of asking for specific individuals and their relations.
Therefore the described workaround by using dummy individuals is unavoidable, and classes need to be
rewritten as in \listingref{lst:suf_class}.


\begin{minipage}{\linewidth}
	\begin{lstlisting}[basicstyle=\small, label=lst:insuf_class, caption={[Insufficient OWL Class]An
		insufficient OWL class for SPARQL queries.}]
    Class: Recommend_Measure_X

    EquivalentTo: 
        hasDiagnosis some Diagnosis_A

    SubClassOf: 
        hasRecommendedMeasure some Measure_X
	\end{lstlisting}
\end{minipage}

\begin{minipage}{\linewidth}
	\begin{lstlisting}[basicstyle=\small, label=lst:suf_class, caption={[A sufficient OWL Class]An
		sufficient OWL class for SPARQL queries where \texttt{R\_Measure\_X} is an instance of class
		\texttt{Measure\_X}.}]
    Individual: R_Measure_X

        Types:
            Measure_X

    Class: Recommend_Measure_X

    EquivalentTo:
        hasDiagnosis some Diagnosis_A

    SubClassOf:
        hasRecommendedMeasure value R_Measure_X
	\end{lstlisting}
\end{minipage}

\section{Programming Tools}
\label{sec:tools}

For this work, CGM/F20 is developed with the following tools: the general management of OWL
ontologies is done with the help of the OWL
API\footnote{\url{https://github.com/owlcs/owlapi://github.com/owlcs/owlapi/}} in version 5.1.2. The
JENA\footnote{\url{https://jena.apache.org/documentation/javadoc/arq://jena.apache.org/documentation/javadoc/arq/}}
library in version 3.7.0 is used to interact with the ontology via SPARQL queries. Furthermore, we
are using the open source reasoner Openllet\footnote{\url{https://github.com/Galigator/openllet}} in
version 2.6.5, which is a fork of Pellet\footnote{\url{https://www.w3.org/2001/sw/wiki/Pellet}} and provides support for the OWL API in version 5.
