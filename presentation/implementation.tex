\section{Implementation}
% Short introduction to OWL

\begin{frame}[fragile]
	\frametitle{Introduction to \emph{Web Ontology Language} (OWL)}
	\begin{block}{General understanding}
		\begin{itemize}
			\item Knowledge representation language
			\item Based on description logics ($\mathcal{ALC}$, $\mathcal{SROIQ}$)
			\item Use-cases: Defining the structure of knowledge for domains of interest
		\end{itemize}
	\end{block}

	\begin{block}{Basic blocks of OWL}
		\begin{itemize}
			\item Instances: Objects (e.g., tim, lisa)
			\item Classes: Set of objects (e.g.,Human(tim), Human(lisa))
			\item Properties: binary relation between objects
				(e.g.,hasParent(tim,lisa));\\
				different logical features: \emph{transitive, symmetric, inverse}
		\end{itemize}
	\end{block}

\end{frame}

\section{Ontological Design}
% Representing drug application
\begin{frame}[fragile]
	\frametitle{Ontology Design I -- Drug Application}
	According to guideline application of drugs is essential for a successful therapy:
	\begin{center}
		\includegraphics[scale=0.65]{concreteDrugExample}
	\end{center}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Ontology Design I -- Drug Application}
	Guidelines defines \emph{starting} and \emph{targeting} dose  for patients:

	\begin{lstlisting}[numbers=none, frame=single,language=xml,morekeywords={and,or,some}]
Class: Startdose_Atypical_Aripiprazole

    EquivalentTo: 
        (concreteDrug some Aripiprazole) and
            (((unity some GramPerDay)
                  and (dose some xsd:double[>= "0.01"^^xsd:double ,
                  <= "0.015"^^xsd:double]))
             or ((unity some MilligramPerDay)
                  and (dose some
                    xsd:double[>= "10.0"^^xsd:double,
                    <= "15.0"^^xsd:double]
                  )
                )
            )
	\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Ontology Design II -- Therapy resistance}
	Representation of a therapy resistance:
	\begin{center}
		\includegraphics[scale=0.5]{concreteDrugResistanceExample}
	\end{center}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Ontology Design II -- Therapy resistance}
	\begin{lstlisting}[numbers=none, frame=single,language=xml,morekeywords={and,or,some,value}]
Class: TherapyResistance_ActiveDrugApplication

  EquivalentTo: 
      hasMeasure some 
          (ApplyAntipsychoticDrug
           and time:CurrentEvent
           and (concreteDrug some (concreteDrugInverse some 
              (DrugResistance
               and time:CurrentEvent
               and (hasDiagnosisInverse some Patient)))))

  SubClassOf: 
      Definite_ResistanceCurrent2ApplyStart,
      RecommendHavingTherapyResistance,
      hasFutureRecommendedMeasure only 
          ((not (Condition_Resistance2CurrentDrug))
          or (time:is_theoretical_next_step_but_not_recommended
              value R_Condition_HadClozapineApplication)),
      hasNonRecommendedMeasure value R_ApplyTargetDose,
      hasNonRecommendedMeasure value R_IncreaseorDecreaseDoseOfAntipsychotica,
      hasRecommendedMeasure value R_MarkDrugApplicationAsNotSuccessful,
      hasRecommendedMeasure value R_StopCurrentDrugApplication
	\end{lstlisting}
\end{frame}

\section{Treatment plans}
% Chronological order of events
\begin{frame}[fragile]
	\frametitle{Chronological Order of Events}
	\begin{itemize}
		\item [$\rightarrow$] As an \textbf{event} we treat measures, diagnosis and symptoms.
		\item [] Generation of a treatment plan requires a \emph{temporal relationship} between
			events.
	\end{itemize}

	\pause

	Introduce two new object properties:
	\begin{center}
		\visible<2>{\includegraphics[scale=0.5]{event_order}}
	\end{center}

\end{frame}

% Future recommendations
\begin{frame}[fragile]
	\frametitle{Future recommendations}
	\alert{Missing}: A relation signalising that a recommendation may be relevant for the therapy of a
	patient

	\vspace*{.5cm}
	$\Rightarrow$ Introduce new object property \texttt{hasFutureRecommendedEvent}

	\pause
	\vspace*{.5cm}

	Create classes for patients that infer these relations to specific recommendations, e.g:
\begin{minipage}{\linewidth}
	\begin{lstlisting}[numbers=none, frame=single,language=xml]
Class: :FutureAcuteSchizophreniaRecommendations

    EquivalentTo:
        :hasFutureRecommendedDiagnosis some :AcuteSchizophrenia

    SubClassOf:
        :hasFutureRecommendedMeasure value R_Liver_First_Week,
        :hasFutureRecommendedMeasure value R_Condition_HasInitialDyskensia,
        :hasFutureRecommendedMeasure value R_MakePhysicalExamination
	\end{lstlisting}
\end{minipage}

\end{frame}


\begin{frame}[fragile]
	\frametitle{Future recommendations}
	\begin{minipage}{\linewidth}
		\begin{lstlisting}[language=xml,numbers=none,frame=single]
Class: :FutureAcuteSchizophreniaRecommendations

    EquivalentTo:
        :hasFutureRecommendedDiagnosis some :AcuteSchizophrenia

    SubClassOf:
        :hasFutureRecommendedMeasure value R_Liver_First_Week,
        :hasFutureRecommendedMeasure value R_Condition_HasInitialDyskensia,
        :hasFutureRecommendedMeasure value R_MakePhysicalExamination
		\end{lstlisting}
	\end{minipage}
	
	The following applies: \alert{$\mathsf{hasRecommendedEvent} \sqsubseteq \mathsf{hasFutureRecommendedEvent}$}

	\begin{block}{}
	$\Rightarrow$ Patients that have an immediate recommendation for a diagnosis of acute
	schizophrenia are member of $\mathsf{:FutureAcuteSchizophreniaRecommendations}$!
	\end{block}

\end{frame}

\begin{frame}
	\frametitle{Future recommendations}
	\begin{center}
		\includegraphics[scale=0.45]{future_event_recommendation}
	\end{center}
	$\Rightarrow$ Future recommendations propagate along a chain of patient classes
\end{frame}

% Conditional events
\begin{frame}
	\frametitle{Conditional events}
	Clinical guidelines consist of many conditions, like:
	\begin{itemize}
		\item In case X is diagnosed then do A \ldots
		\item In case Y has been applied without success then do B \ldots
		\item In case an application Z needs to be changed then do C \ldots
	\end{itemize}

	$\Rightarrow$ Introducing conditional events and object properties for \textbf{true} and
	\textbf{false} case:
	\begin{center}
		\includegraphics[scale=0.35]{conditional_event}
	\end{center}
\end{frame}

% Definite knowledge

\begin{frame}[fragile]
	\frametitle{Definite knowledge}
	Treatment plan needs to adjust dynamically whenever possible.
	\vspace*{5pt}
	
	\only<1>{Plan when therapy resistance information is \alert{not provided}:}
	\only<2>{Plan when therapy resistance information is \alert{provided}:}
	\begin{center}
		\only<1>{\includegraphics[width=\textwidth, height=6.5cm]{patient_before_resistance}}
		\only<2>{\includegraphics[width=\textwidth]{patient_after_resistance}}
	\end{center}
	\only<2>{$\Rightarrow\alert{\mathsf{is\_next\_step\_because\_condition\_is\_}}$\alert{\{}$\alert{\mathsf{true,false}}$\alert{\}}
	communicate truth-value.}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Definite knowledge}
	\begin{block}{Infer Knowledge}
\begin{align*}
	\forall&\mathsf{hasFutureRecommendedEvent}\ldotp\\
	&\text{(}\neg \text{(}\mathsf{Condition\_PatientHasADrugResistance}\text{)}\vee\\
	&\text{(}\exists\color<3>{red}{\mathsf{time:is\_next\_step\_because\_condition\_is\_true}}\ldotp\\
	&\mathsf{R\_StartCognitiveBehaviouralTherapy}\text{))}
\end{align*}
	\end{block}
	\pause

	\begin{figure}[H]
		\begin{center}
			\begin{tikzpicture}
				\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
				\node (B) at (0,0) {patient};
				\node (D) at (0.4,-1.0) {$\udots$};
				\node (E) at (1.8,-1.7) {$\mathsf{SomeMeasureX}$};
				\node (F) at (0,-2.2) {$\mathsf{SomeMeasureY}$};
				\node (C) at (6,0) {$\mathsf{Condition\_PatientHasADrugResistance}$};

				\path[->] (B) edge  node[below] {$\mathsf{hasFutureRec}$} (C);
				\path[->] (B) edge  node[right] {$\mathsf{hasFutureRec}$} (E);
				\path[->] (B) edge  node[left] {$\mathsf{hasFutureRec}$} (F);
				\node (F) at (6,-3) {{\visible<3>{$\mathsf{R\_StartCognitiveBehaviouralTherapy}$}}};
				\path[dashed,->,visible on=<3>] (C) edge [red] node[fill=white]
				{$\mathsf{time:is\_next\_step\ldots}$} (F);
			\end{tikzpicture}
		\end{center}
	\end{figure}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Multi-Level Conditions}
	Some conditions depend on other factors, as well:
	\begin{center}
	\includegraphics[scale=0.31]{../../img/implementation/multiple_options}
	\end{center}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Multi-Level Conditions}
	Therefore, introduce another pair of object properties:
	$\alert{\mathsf{is\_possible\_next\_step\_when\_}}$\alert{\{}$\alert{\mathsf{true,false}}$\alert{\}$\_\mathsf{multi}$}
	\begin{center}
	\includegraphics[width=\textwidth]{../../img/implementation/multiple_options2}
	\end{center}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Repeating Events}
	Measures repeat in specific time steps, e.g. Blood count
	\begin{center}
		\begin{tabular}{|l||c|c|c|c|c|}
			\hline
			Measure           & Start & after 4 weeks & after 3 months & every 3 months\\
			\hline
			Blood count       & x     & x             & x              & x  \\
			\hline
		\end{tabular}
	\end{center}
	Treatment plan should display prediction of next schedule:
	\begin{center}
		\includegraphics[scale=0.5]{patient_future_bloodcount}
	\end{center}
\end{frame}


\begin{frame}[fragile]
	\frametitle{Repeating Events}
	\begin{figure}[H]
		\begin{center}
			\begin{tikzpicture}
				\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
				\node (B) at (0,0) {patient};
				\node (D) at (6,0) {Bloodcount\_1};
				\node (E) at (6,-2.0) {$3$};
				\node (C) at (0,-2.0) {R\_Bloodcount\_EveryThreeMonths};

				\path[->] (B) edge  node[above] {$\mathsf{hasMeasure}$} (D);
				\path[->] (D) edge  node[right] {$\mathsf{isCompletedSince}$} (E);
				\path[dashed,->,visible on=<2>,bend angle=45, bend left] (D) edge [BurntOrange] node[below,fill=white]
				{$\mathsf{repeatingIn3Months}$} (C);
				\path[dashed,->,visible on=<2>] (B) edge [BlueGreen] node[fill=white]
				{$\mathsf{hasNonRecommendedMeasure}$} (C);
			\end{tikzpicture}
		\end{center}
	\end{figure}
	\pause

	\begin{lstlisting}[numberstyle=\scriptsize,language=xml,identifierstyle=\ttfamily,keywordstyle=\ttfamily,
	escapechar=!]
Class: Opposite_Repeating_BloodCount_EveryThreeMonths
    EquivalentTo:
         (hasMeasure some (Bloodcount_4Weeks
             and (time:isCompletedSince some [< "90"])))

    SubClassOf:
        hasMeasure only
            ((not (Bloodcount and (time:isCompletedSince some [< "90"])))
              or (!\color{BurntOrange}{time:repeatingIn3Months}! value R_Bloodcount_EveryThreeMonths)),
        !\color{BlueGreen}{hasNonRecommendedMeasure}! value R_Bloodcount_EveryThreeMonths
	\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Repeating Events}
	SWRL Rule:
	\begin{lstlisting}[numbers=none,mathescape=true,frame=single,escapechar=!]
	!\color{BurntOrange}{time:repeatingIn3Months(?a, ?rec)}! $\wedge$ time:isCompletedSince(?a, ?completed) $\wedge$
	swrlb:sub(?rep, 90, ?completed) $\to$ !\color{red}{time:isRepeatingIn(?rec, ?rep)}!
	\end{lstlisting}
	\pause
	\begin{figure}[H]
		\begin{center}
			\begin{tikzpicture}
				\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
				\node (B) at (0,0) {patient};
				\node (D) at (6,0) {Bloodcount\_1};
				\node (E) at (6,-2.0) {$3$};
				\node (C) at (0,-2.0) {R\_Bloodcount\_EveryThreeMonths};
				\node (F) at (0,-4.0) {$87$};

				\path[->] (B) edge  node[above] {$\mathsf{hasMeasure}$} (D);
				\path[->] (D) edge  node[right] {$\mathsf{isCompletedSince}$} (E);
				\path[dashed,->,bend angle=45, bend left] (D) edge [BurntOrange] node[below,fill=white]
				{$\mathsf{repeatingIn3Months}$} (C);
				\path[dashed,->] (B) edge [BlueGreen] node[fill=white]
				{$\mathsf{hasNonRecommendedMeasure}$} (C);
				\path[dashed,->,bend angle=45, bend right] (C) edge [red] node[below,fill=white]
				{$\mathsf{isRepeatingIn}$} (F);
			\end{tikzpicture}
		\end{center}
	\end{figure}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Repeating Events}
	\begin{lstlisting}[numbers=none, mathescape=true, frame=single,escapechar=!]
	!\color{BlueGreen}{:hasNonRecommendedEvent(?patient, ?ev)}! $\wedge$ !\color{red}{time:isRepeatingIn(?ev, ?x)}!$\to$
	!\color{Sepia}{:hasRepeatingRecommendedEvent(?patient, ?ev)}!
	\end{lstlisting}
	\pause
	\begin{figure}[H]
		\begin{center}
			\begin{tikzpicture}
				\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
				\node (B) at (0,0) {patient};
				\node (D) at (6,0) {Bloodcount\_1};
				\node (E) at (6,-2.0) {$3$};
				\node (C) at (0,-2.0) {R\_Bloodcount\_EveryThreeMonths};
				\node (F) at (0,-4.0) {$87$};

				\path[->] (B) edge  node[above] {$\mathsf{hasMeasure}$} (D);
				\path[->] (D) edge  node[right] {$\mathsf{isCompletedSince}$} (E);
				\path[dashed,->,bend angle=45, bend left] (D) edge [BurntOrange] node[below,fill=white]
				{$\mathsf{repeatingIn3Months}$} (C);
				\path[dashed,->, bend angle=45, bend left] (B) edge [BlueGreen] node[fill=white]
				{$\mathsf{B}$} (C);
				\path[dashed,->,bend angle=45, bend right] (C) edge [red] node[below,fill=white]
				{$\mathsf{isRepeatingIn}$} (F);
				\path[dashed,->,bend angle=45, bend right] (B) edge [Sepia] node[left,fill=white]
				{A} (C);
			\end{tikzpicture}
		\end{center}
	\end{figure}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Queries I}
	Querying is done via SPARQL:
	\begin{itemize}
		\item A language similar to SQL.
		\item Able to query inferred knowledge-base.
		\item Closed-World assumption.
	\end{itemize}

	Query for repeating measures:
	\begin{lstlisting}[language=sql,morekeywords={OPTIONAL,PREFIX},frame=single,numbers=none]
PREFIX :<http://www8.cs.fau.de/research:cgm/schizophrenia#>
PREFIX time:<http://www8.cs.fau.de/research:cgm/time#>

SELECT ?treatment (MAX(?timestamp) as ?greatestTimestamp)
	WHERE {
	    ?patient :hasRepeatingRecommendedMeasure ?treatment.
	    ?treatment time:isRepeatingIn ?timestamp.
	}
	Group By ?treatment
	\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Queries II}
	For receive necessary information for the treatment plan the following query is used:

\begin{minipage}{\linewidth}
	\begin{lstlisting}[numbers=none,frame=single,basicstyle=\tiny,language=sql,deletekeywords={NEXT},deletendkeywords={TIME},morekeywords={OPTIONAL,PREFIX}]
PREFIX :<http://www8.cs.fau.de/research:cgm/schizophrenia#>
PREFIX time:<http://www8.cs.fau.de/research:cgm/time#>

SELECT ?treatment ?next ?nextFalse ?nextTrue ?definiteFalse ?definiteTrue
       ?pathFalse ?pathTrue ?non_recommended
WHERE{
    ?patient :hasFutureRecommendedEvent ?treatment.
    OPTIONAL {?treatment time:beforeNextRecommendedEvent ?next}
    OPTIONAL {?treatment time:is_possible_next_step_when_false ?nextFalse}
    OPTIONAL {?treatment time:is_possible_next_step_when_true  ?nextTrue}
    OPTIONAL {?treatment time:is_next_step_because_condition_is_false ?definiteFalse}
    OPTIONAL {?treatment time:is_next_step_because_condition_is_true ?definiteTrue}
    OPTIONAL {?treatment time:is_possible_next_step_when_false ?externalFalse}
    OPTIONAL {?treatment time:is_possible_next_step_when_true_multi ?externalTrue}
    OPTIONAL {?treatment time:is_theoretical_next_step_but_not_recommended ?non_recommended}
}
	\end{lstlisting}
\end{minipage}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Treatment plan generation}

	\begin{algorithm}[H]

		\begin{algorithmic}[1]
			\Procedure{CreateTreatmentPlan}{Patient p}
			\State $\text{recs} \gets \text{queryForRecommendations(p)}$
			\Comment Ask for next recommendations
			\State $\text{futureRecs} \gets \text{queryForFutureRecommendatins}\text{(p)}$
			\Comment Ask for future recommendations

			\State $\text{g := newGraph()}$
			\State $\text{q := newQueue()}$
			\State $\text{visited := newSet()}$
			\State $\text{g.addVertex(p)}$
			\For{$\text{rec} \gets \text{recs}$}\Comment Add next recommendations
			\State $\text{g.addEdge(p, rec)}$
			\State $\text{q.add(rec)}$
			\EndFor

			\While{$(\text{!q.isEmpty())}$}\Comment Loop until no more recommendations are left
			\State $\text{nextRec} \gets \text{q.pop()}$
			\If{$\text{visited.contains(nextRec)}$}
			\State $\text{continue}$
			\Else
			\State $\text{visited.add(nextRec)}$
			\EndIf
			\State $\text{succs} \gets \text{getAllNeccessarySuccessors(nextRec, futureRecs)}$\Comment Filter successors
			\For{$\text{succ} \gets \text{succs}$}
			\State $\text{g.addEdge(nextRec, succ)}$
			\State $\text{q.push(succ)}$
			\EndFor
			\EndWhile
			\EndProcedure
		\end{algorithmic}
	\end{algorithm}
\end{frame}
