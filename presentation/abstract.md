In this talk I will present the results of my master thesis.
It is about extending the Clinical Support System, CGM/F20, to generate
individual treatment plans for patients. In contrast to existing work in this field, the treatment
plan can adapt dynamically depending on the given circumstances.
The generation is based on an ontological knowledge-base, which consists of a guideline for
schizophrenia as well as medical records for patients. Instead of inferring only recommendations for
an immediate application, the existing knowledge is used to generate a treatment plan that is as
specific as possible.
In particular, the ontology has been extended regarding the temporal relationship between events
and their recommendation strength. Besides transitioning a large part of the guideline into an
ontology, other tools like SPARQL queries and SWRL rules have been used.
Besides explaining the basic ideas of my approach, I will present a little case study as well.
